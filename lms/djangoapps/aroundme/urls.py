from django.conf.urls import patterns, url
from django.conf import settings

urlpatterns = patterns(
    'aroundme.views',
    url(r"^{}/sendmessage$".format(settings.COURSE_ID_PATTERN), 'send_message', name="send_message")
)
