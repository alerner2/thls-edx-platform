from django.db.models import Q
from xmodule.modulestore.django import modulestore
from django.contrib.auth.models import User
from openedx.core.djangoapps.user_api.accounts.api import get_account_settings

from student.models import CourseEnrollment


def get_public_users(request, course_key, page, sort_pref, selected_country, query):
    paginator = 10
    start = (page-1) * paginator
    end = paginator * page
    with modulestore().bulk_operations(course_key):
        user_list = CourseEnrollment.objects.filter(course_id=course_key).values_list('user__id', flat=True)
        if sort_pref == 'city':
            if query:
                users = User.objects.filter(
                    Q(id__in=user_list) &
                    Q(preferences__key='account_privacy') &
                    Q(preferences__value='all_users') &
                    Q(profile__country=selected_country) &
                    (Q(profile__city__icontains=query) | Q(username__icontains=query))
                ).order_by('profile__city')[start:end]
            else:
                users = User.objects.filter(
                    id__in=user_list,
                    preferences__key='account_privacy',
                    preferences__value='all_users',
                    profile__country=selected_country
                ).order_by('profile__city')[start:end]
        else:
            if query:
                users = User.objects.filter(
                    Q(id__in=user_list) &
                    Q(preferences__key='account_privacy') &
                    Q(preferences__value='all_users') &
                    Q(profile__country=selected_country) &
                    (Q(profile__city__icontains=query) | Q(username__icontains=query))
                ).order_by('username')[start:end]
            else:
                users = User.objects.filter(
                    id__in=user_list,
                    preferences__key='account_privacy',
                    preferences__value='all_users',
                    profile__country=selected_country
                ).order_by('username')[start:end]
        public_users = []
        for user in users:
            account_settings_data = get_account_settings(request, [user.username])[0]
            user_data = {
                'username': account_settings_data.get('username'),
                'city': account_settings_data.get('city', ''),
                'bio': account_settings_data.get('bio', ''),
                'profile_pic': account_settings_data.get('profile_image').get('image_url_large')
            }
            if not user_data.get('city'):
                user_data['city'] = 'Aucune'
            if not user_data.get('bio'):
                user_data['bio'] = 'Pas d\'information'
            public_users.append(user_data)

        return public_users
