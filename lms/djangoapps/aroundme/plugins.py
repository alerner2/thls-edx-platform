from xmodule.tabs import CourseTab
from django.utils.translation import ugettext_noop


class AroundMeTab(CourseTab):
    """
    The Around me view.
    """
    type = 'around_me'
    title = ugettext_noop('Who\'s Who')
    priority = 20
    view_name = 'around_me'
    tab_id = 'around_me'
    is_movable = True
    is_default = False
    is_dynamic = True

    @classmethod
    def is_enabled(cls, course, user=None):
        return True
