import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST
from opaque_keys.edx.locations import SlashSeparatedCourseKey
from xmodule.modulestore.django import modulestore
from django.contrib.auth.models import User
from django_countries import countries
from aroundme.helpers import get_public_users
from instructor.access import list_with_level

from util.views import ensure_valid_course_key
from courseware.courses import get_course_with_access
from edxmako.shortcuts import render_to_response


@login_required
@ensure_csrf_cookie
@ensure_valid_course_key
def user_list(request, course_id):
    course_key = SlashSeparatedCourseKey.from_deprecated_string(course_id)
    selected_country = request.GET.get('country', 'FR')
    sort_pref = request.GET.get('sort', 'username')
    query = request.GET.get('query', '').strip()
    page = int(request.GET.get('page', 1))
    users = get_public_users(request, course_key, page, sort_pref, selected_country, query)
    data = {
        'result': users
    }
    if not users:
        data['last_page'] = True
    return HttpResponse(json.dumps(data), content_type='application/json')


@login_required
@ensure_csrf_cookie
@ensure_valid_course_key
def around_me(request, course_id):
    """
    Display the course's info.html, or 404 if there is no such course.

    Assumes the course_id is in a valid format.
    """
    course_key = SlashSeparatedCourseKey.from_deprecated_string(course_id)
    selected_country = request.GET.get('country', 'FR')
    sort_pref = request.GET.get('sort', 'username')
    query = request.GET.get('query', '').strip()
    with modulestore().bulk_operations(course_key):
        course = get_course_with_access(request.user, 'load', course_key)

        available_countries = list(countries)
        context = {
            'selected_country': selected_country,
            'sort_pref': sort_pref,
            'course': course,
            'course_id': course_id,
            "countries": available_countries,
            "query": query,
            "users": get_public_users(request, course_key, 1, sort_pref, selected_country, query)
        }
        return render_to_response('aroundme/around-me.html', context)


@login_required
@ensure_csrf_cookie
@ensure_valid_course_key
@require_POST
def send_message(request, course_id):
    data = {}
    course_key = SlashSeparatedCourseKey.from_deprecated_string(course_id)
    with modulestore().bulk_operations(course_key):
        course = get_course_with_access(request.user, 'load', course_key)
        course_display_name = course.display_name_with_default
        if 'username' in request.POST and 'message' in request.POST:
            username = request.POST.get('username')
            message = request.POST.get('message')
            user = get_object_or_404(User, username=username)
            subject = u"Nouveau message de {} dans {}".format(
                request.user.username,
                course_display_name
            )
            from_email = settings.DEFAULT_FROM_EMAIL
            instructors = list_with_level(course, 'instructor')
            bcc_email = []
            for instructor in instructors:
                bcc_email.append(instructor.email)
            cc_email = request.user.email
            email = EmailMultiAlternatives(subject, message, from_email, [user.email], bcc=bcc_email, cc=[cc_email])
            email.send()
            data["success"] = True
        else:
            data["success"] = False
        return HttpResponse(json.dumps(data), content_type='application/json')
